package database

type ImmutableStatusEffect struct {
	statusEffect *StatusEffect
}

func (i ImmutableStatusEffect) StatusEffect() StatusEffect {
	return *i.statusEffect
}

func MakeImmutableStatusEffect(statusEffect *StatusEffect) ImmutableStatusEffect {
	return ImmutableStatusEffect{statusEffect}
}

type ImmutableActionType struct {
	actionType *ActionType
}

func (i ImmutableActionType) ActionType() ActionType {
	return *i.actionType
}

func MakeImmutableActionType(actionType *ActionType) ImmutableActionType {
	return ImmutableActionType{actionType}
}

type ImmutableRace struct {
	race *Race
}

func (i ImmutableRace) Race() Race {
	return *i.race
}

func MakeImmutableRace(race *Race) ImmutableRace {
	return ImmutableRace{race}
}

type ImmutableSkill struct {
	skill *Skill
}

func (i ImmutableSkill) Skill() Skill {
	return *i.skill
}
func MakeImmutableSkill(skill *Skill) ImmutableSkill {
	return ImmutableSkill{skill: skill}
}

type ImmutableDemon struct {
	demon *Demon
}

func (i ImmutableDemon) Demon() Demon {
	return *i.demon
}

func MakeImmutableDemon(demon *Demon) ImmutableDemon {
	return ImmutableDemon{demon: demon}
}
