package database

type StatusEffect struct {
	Name   string
	Effect string
	Game   Game
}
