package database

// Skill skill that a demon can be performed
type Skill struct {
	Name           string
	ActionType     *ImmutableActionType
	Cost           Cost
	StatusEffect   []StatusEffectDescriptor
	Target         Target
	Game           Game
	Dodge          Chance
	Critical       Chance
	ExclusiveSkill []*ImmutableDemon
	MaxNumberOfHit int
	Damage         Damage
	Description    string
	Buff           []BuffDescriptor
}

type BuffDescriptor struct {
	Multiplier float32
	Buff       Buff
}

// StatusEffectDescriptor describe the status effect and it's odd
type StatusEffectDescriptor struct {
	StatusEffect ImmutableStatusEffect
	Percentage   int
}

// cost of a skill that require a self sacrifice
const SACRIFICE int = -999

// numerical information is unknown
const UNKNOWN int = -888

// the skill have no cost
const NO_COST int = -777

// Target target of the skill
type Target int

// possible target
const (
	Self Target = iota
	Single
	All
	Random
	Universal
	SingleAlly
	AllAlly
	RandomAll
)

// Cost cost of a skill
type Cost struct {
	Unit  Unit // unit of the cost
	Value int  // value of the cost
}

// Origin describe by the way and the lv by wich the skill have been aquire
type Origin struct {
	Lv        int
	WayAquire WayAquire
}

// wayAquire describe the way the skill have been aquire
type WayAquire int

// possible way the skill is aquire
const (
	Innate WayAquire = iota
	Learned
	Fusion
)

// Unit unit of the cost of the skill
type Unit int

// possible unit
const (
	Mp Unit = iota
	Passive
	PercentageHp
)

// Chance chance that the skill connect
type Chance int

// Possible odd
const (
	High Chance = iota
	Medium
	Low
	Impossible
	NotProvided
)

// Damage damage of the skill deal
type Damage int

// possible damage
const (
	Light Damage = iota
	Moderate
	Heavy
	Severe
	Mega
	Fully
	No
)
