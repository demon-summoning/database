package database

import "fmt"

type MoonPhase struct {
	number int
}

func NewMoonPhase(number int) (MoonPhase, error) {
	if number > 8 || number < 0 {
		return MoonPhase{}, fmt.Errorf("%d is not a phase of the moon", number)
	}
	return MoonPhase{number: number}, nil
}
