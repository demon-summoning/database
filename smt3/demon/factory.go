package demon

import domain "gitlab.com/demon-summoning/database"

// undefined behavior if name not is not the const value defined in this package
func ProvideADemon(name DemonName) *domain.ImmutableDemon {
	demon := allDemon[name]
	return &demon
}
