package demon

import domain "gitlab.com/demon-summoning/database"

var skadi = domain.Demon{}

var kikuriHime = domain.Demon{}

var ladies = map[DemonName]domain.ImmutableDemon{
	Skadi:      domain.MakeImmutableDemon(&skadi),
	KikuriHime: domain.MakeImmutableDemon(&kikuriHime),
}
