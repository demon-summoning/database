package demon

import domain "gitlab.com/demon-summoning/database"

var pazuzu domain.Demon = domain.Demon{}

var viles = map[DemonName]domain.ImmutableDemon{
	Pazuzu: domain.MakeImmutableDemon(&pazuzu),
}
