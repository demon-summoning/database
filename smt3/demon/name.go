package demon

type DemonName int

const (
	//fiend
	DemiFiend DemonName = iota
	Matador
	HellBiker
	RedRider
	Dante
	Trumpeter
	// unknown
	RaidouKuzunoha
	// tyrant
	Loki
	Surt
	// wargod
	Valkyrie
	// deity
	Mithra
	// lady
	Skadi
	KikuriHime
	//vile
	Pazuzu
	// fury
	Dionysus
)
