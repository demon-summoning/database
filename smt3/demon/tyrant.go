package demon

import domain "gitlab.com/demon-summoning/database"

var surt domain.Demon = domain.Demon{}
var loki domain.Demon = domain.Demon{}

var tyrants = map[DemonName]domain.ImmutableDemon{
	Surt: domain.MakeImmutableDemon(&surt),
	Loki: domain.MakeImmutableDemon(&loki),
}
