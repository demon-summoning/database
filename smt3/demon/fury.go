package demon

import (
	domain "gitlab.com/demon-summoning/database"
)

var dionysus = domain.Demon{}

var furies = map[DemonName]domain.ImmutableDemon{
	Dionysus: domain.MakeImmutableDemon(&dionysus),
}
