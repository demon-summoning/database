package demon

import domain "gitlab.com/demon-summoning/database"

var valkyrie = domain.Demon{}

var wargods = map[DemonName]domain.ImmutableDemon{
	Valkyrie: domain.MakeImmutableDemon(&valkyrie),
}
