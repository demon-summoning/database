package demon

import (
	domain "gitlab.com/demon-summoning/database"
)

var demiFiend = domain.Demon{}

var matador = domain.Demon{}

var hellBiker = domain.Demon{}

var redRider = domain.Demon{}

var dante = domain.Demon{}

var trumpeter = domain.Demon{}

var fiends = map[DemonName]domain.ImmutableDemon{
	DemiFiend: domain.MakeImmutableDemon(&demiFiend),
	Matador:   domain.MakeImmutableDemon(&matador),
	HellBiker: domain.MakeImmutableDemon(&hellBiker),
	RedRider:  domain.MakeImmutableDemon(&redRider),
	Dante:     domain.MakeImmutableDemon(&dante),
	Trumpeter: domain.MakeImmutableDemon(&trumpeter),
}
