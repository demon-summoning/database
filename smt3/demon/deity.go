package demon

import (
	domain "gitlab.com/demon-summoning/database"
)

var mithra = domain.Demon{}

var deitys = map[DemonName]domain.ImmutableDemon{
	Mithra: domain.MakeImmutableDemon(&mithra),
}
