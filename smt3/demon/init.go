package demon

import domain "gitlab.com/demon-summoning/database"

var allDemon map[DemonName]domain.ImmutableDemon

func init() {
	for k, v := range fiends {
		allDemon[k] = v
	}
	for k, v := range unknowns {
		allDemon[k] = v
	}
	for k, v := range tyrants {
		allDemon[k] = v
	}
	for k, v := range wargods {
		allDemon[k] = v
	}
	for k, v := range deitys {
		allDemon[k] = v
	}
	for k, v := range ladies {
		allDemon[k] = v
	}
	for k, v := range furies {
		allDemon[k] = v
	}
	for k, v := range viles {
		allDemon[k] = v
	}
}
