package demon

import domain "gitlab.com/demon-summoning/database"

var raidouKuzunoha = domain.Demon{}

var unknowns = map[DemonName]domain.ImmutableDemon{
	RaidouKuzunoha: domain.MakeImmutableDemon(&raidouKuzunoha),
}
