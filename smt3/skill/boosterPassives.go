package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var fireBoost = domain.Skill{
	Name:       "fire boost",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "boost fire skill",
}

var iceBoost = domain.Skill{
	Name:       "ice boost",
	ActionType: smt3.ProvideActionType(smt3.Ice),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "boost ice skill",
}

var forceBoost = domain.Skill{
	Name:       "force boost",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "boost force skill",
}

var elecBoost = domain.Skill{
	Name:       "elec boost",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var sonsOath = domain.Skill{
	Name:       "son's oath",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases potency of all attacks by 50%, gives pierce to physical and almighty physical attacks",
	Buff: []domain.BuffDescriptor{
		domain.BuffDescriptor{Multiplier: 0.5, Buff: domain.AttackUp},
		domain.BuffDescriptor{Multiplier: 0.5, Buff: domain.MagicUp},
	},
}

var raidouTheAeon = domain.Skill{
	Name:       "raidou the aeon",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases potency of all attacks by 50%, gives pierce to physical and almighty physical attacks",
	Buff: []domain.BuffDescriptor{
		domain.BuffDescriptor{Multiplier: 0.5, Buff: domain.AttackUp},
		domain.BuffDescriptor{Multiplier: 0.5, Buff: domain.MagicUp},
	},
}

var boosterPassives = map[NameSkill]domain.ImmutableSkill{
	FireBoost:     domain.MakeImmutableSkill(&fireBoost),
	IceBoost:      domain.MakeImmutableSkill(&iceBoost),
	ForceBoost:    domain.MakeImmutableSkill(&forceBoost),
	ElecBoost:     domain.MakeImmutableSkill(&elecBoost),
	SonsOath:      domain.MakeImmutableSkill(&sonsOath),
	RaidouTheAeon: domain.MakeImmutableSkill(&raidouTheAeon),
}
