package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func BrutalSlash() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 13}
	return demon.Skill{
		Name:           "brutal slash",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Medium,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func MightyGust() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 14}
	return demon.Skill{
		Name:           "mighty gust",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Medium,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Guillotine() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 17}

	return demon.Skill{
		Name:           "guillotine",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Medium,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func DarkSword() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 22}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Mute(),
		Percentage:   40,
	}

	return demon.Skill{
		Name:           "dark sword",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.High,
		Critical:       demon.High,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func StasisBlade() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 22}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Bind(),
		Percentage:   65,
	}

	return demon.Skill{
		Name:           "stasis blade",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.High,
		Critical:       demon.High,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func HeatWave() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 16}

	return demon.Skill{
		Name:           "heat wave",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.All,
		Game:           domain.Smt3,
		Dodge:          demon.Low,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Blight() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 36}

	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Poison(),
		Percentage:   20,
	}

	return demon.Skill{
		Name:           "blight",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.All,
		Game:           domain.Smt3,
		Dodge:          demon.Low,
		Critical:       demon.Low,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Hassohappa() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 35}

	return demon.Skill{
		Name:           "heat wave",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.All,
		Game:           domain.Smt3,
		Dodge:          demon.Low,
		Critical:       demon.High,
		MaxNumberOfHit: 1,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func ChaosBlade() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 32}

	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Panic(),
		Percentage:   30,
	}
	return demon.Skill{
		Name:           "chaos blade",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Random,
		Game:           domain.Smt3,
		Dodge:          demon.Low,
		Critical:       demon.Low,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Deathbound() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 25}

	return demon.Skill{
		Name:           "deathbound",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Random,
		Game:           domain.Smt3,
		Dodge:          demon.Low,
		Critical:       demon.Low,
		MaxNumberOfHit: 1,
		Damage:         demon.Moderate,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
