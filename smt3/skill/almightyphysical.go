package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var lastResort = domain.Skill{
	Name:       "last resort",
	ActionType: smt3.ProvideActionType(smt3.AlmightyPhysical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 100,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Universal,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}
var kamikaze = domain.Skill{
	Name:       "kamikaze",
	ActionType: smt3.ProvideActionType(smt3.AlmightyPhysical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 100,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
}
var sacrifice = domain.Skill{
	Name:       "sacrifice",
	ActionType: smt3.ProvideActionType(smt3.AlmightyPhysical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 100},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var freikugel = domain.Skill{
	Name:       "freikugel",
	ActionType: smt3.ProvideActionType(smt3.AlmightyPhysical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 17},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var stinger = domain.Skill{
	Name:       "stinger",
	ActionType: smt3.ProvideActionType(smt3.AlmightyPhysical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 12},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}

var almightyPhysicals = map[NameSkill]domain.ImmutableSkill{
	LastResort: domain.MakeImmutableSkill(&lastResort),
	Kamikaze:   domain.MakeImmutableSkill(&kamikaze),
	Sacrifice:  domain.MakeImmutableSkill(&sacrifice),
	Freikugel:  domain.MakeImmutableSkill(&freikugel),
	Stinger:    domain.MakeImmutableSkill(&stinger),
}
