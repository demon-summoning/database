package skill

type NameSkill int

const (
	//almighty physical
	LastResort NameSkill = iota
	Kamikaze
	Sacrifice
	Freikugel
	Stinger
	//attack affinity passives
	AntiPhys
	AntiFire
	AntiIce
	AntiForce
	AntiElec
	AntiMind
	AntiNerve
	AntiCurse
	AntiExpel
	AntiDeath
	VoidPhys
	VoidFire
	VoidIce
	VoidForce
	VoidElec
	VoidMind
	VoidNerve
	VoidCurse
	VoidExpel
	VoidDeath
	PhysDrain
	FireDrain
	IceDrain
	ForceDrain
	ElecDrain
	PhysRepel
	FireRepel
	IceRepel
	ForceRepel
	ElecRepel
	//booster pasives
	FireBoost
	IceBoost
	ForceBoost
	ElecBoost
	SonsOath
	RaidouTheAeon
	//buffs
	Tarukaja
	Makakaja
	Rakukaja
	Sukukaja
	Focus
	RedCapote
	//canceler
	Dekunda
	Dekaja
	HolyStar
	GuardianRaptor
	// claw
	IronClaw
	StunClaw
	VenomClaw
	FeralClaw
	// conversation
	Talk
	JiveTalk
	Scout
	Kidnap
	Beseech
	Brainwash
	Wooing
	Seduce
	DarkPledge
	Mischief
	SoulRecruit
	DeathPact
	// debuff
	Tarunda
	Sukunda
	Rakunda
	WarCry
	FogBreath
	Taunt
	Debilitate
	Provoke
	// electric
	Zio
	Zionga
	Ziodyne
	Mazio
	Mazionga
	Maziodyne
	Shock
	BoltStorm
	RoundTrip
	MishagujiThunder
	// exclusive physical
	DivineShot
	SpiralViper
	OniKagura
	DeadlyFury
	JavelinRain
	XerosBeat
	GaeaRage
	Earthquake
	Andalucia
	HellSpin
	Terrorblade
	Rebellion
	E_I
	BulletTime
	BoogieWoogie
	YoshitsuneShowUp
	MokoiBoomerang
	// fire
	Agi
	Agilao
	Agidyne
	Maragi
	Maragion
	Maragidyne
	FireBreath
	HellFire
	Prominence
	Ragnarok
	HellBurner
	MagmaAxis
	// force
	Zan
	Zanma
	Zandyne
	Mazan
	Mazanma
	WingBuffet
	Tornado
	WindCutter
	HellExhaust
	WetWind
	WhirlWind
	HitokotoGust
	// healing
	Dia
	Media
	Diarama
	Mediarama
	Diarahan
	Mediarahan
	Posumidi
	Mutudi
	Paraladi
	Petradi
	Patra
	MePatra
	Recarm
	Samarecarm
	Recarmdra
	Prayer
	HolyMelody
	// ice
	Bufu
	Bufula
	Bufudyne
	Mabufu
	Mabufula
	Mabufudyne
	IceBreath
	GlacialBlast
	// interruption
	Persuade
	Nag
	Flatter
	Intimidate
	Gonnection
	Haggle
	Detain
	Arbitration
	MaidensPlea
	WineParty
)
