package skill

import domain "gitlab.com/demon-summoning/database"

var skills map[NameSkill]domain.ImmutableSkill

func init() {
	for k, v := range almightyPhysicals {
		skills[k] = v
	}
	for k, v := range attackAffinityPassives {
		skills[k] = v
	}
	for k, v := range boosterPassives {
		skills[k] = v
	}
	for k, v := range buffs {
		skills[k] = v
	}
	for k, v := range cancelers {
		skills[k] = v
	}
	for k, v := range claws {
		skills[k] = v
	}
	for k, v := range conversations {
		skills[k] = v
	}
	for k, v := range debuffs {
		skills[k] = v
	}
	for k, v := range electrics {
		skills[k] = v
	}
	for k, v := range exclusivePhysicals {
		skills[k] = v
	}
	for k, v := range fires {
		skills[k] = v
	}
	for k, v := range forces {
		skills[k] = v
	}
	for k, v := range healings {
		skills[k] = v
	}
}
