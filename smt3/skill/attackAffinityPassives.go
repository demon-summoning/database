package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
)

var antiPhys = domain.Skill{
	Name:       "anti-phys",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 0,
	Damage:         domain.No,
	Description:    "resist physical attack",
}

var antiFire = domain.Skill{
	Name:       "anti-fire",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist fire attack",
}

var antiIce = domain.Skill{
	Name:       "anti-ice",
	ActionType: smt3.ProvideActionType(smt3.Ice),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist ice attack",
}

var antiForce = domain.Skill{
	Name:       "anti-force",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist force attack",
}

var antiElec = domain.Skill{
	Name:       "anti-elec",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist elec attack",
}

var antiMind = domain.Skill{
	Name:       "anti-mind",
	ActionType: smt3.ProvideActionType(smt3.Mind),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist mind attack",
}

var antiNerve = domain.Skill{
	Name:       "anti-nerve",
	ActionType: smt3.ProvideActionType(smt3.Nerve),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist nerve attack",
}

var antiCurse = domain.Skill{
	Name:       "anti-curse",
	ActionType: smt3.ProvideActionType(smt3.Curse),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist curse attack",
}

var antiExpel = domain.Skill{
	Name:       "anti-expel",
	ActionType: smt3.ProvideActionType(smt3.Expel),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist curse attack",
}

var antiDeath = domain.Skill{
	Name:       "anti-death",
	ActionType: smt3.ProvideActionType(smt3.Death),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "resist curse attack",
}

var voidPhys = domain.Skill{
	Name:       "void phys",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void physical attack",
}

var voidFire = domain.Skill{
	Name:       "void fire",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void fire attack",
}

var voidIce = domain.Skill{
	Name:       "void ice",
	ActionType: smt3.ProvideActionType(smt3.Ice),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void ice attack",
}

var voidForce = domain.Skill{
	Name:       "void force",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void force attack",
}

var voidElec = domain.Skill{
	Name:       "void elec",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void elec attack",
}

var voidMind = domain.Skill{
	Name:       "void mind",
	ActionType: smt3.ProvideActionType(smt3.Mind),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void mind attack",
}

var voidNerve = domain.Skill{
	Name:       "void nerve",
	ActionType: smt3.ProvideActionType(smt3.Nerve),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void nerve attack",
}

var voidCurse = domain.Skill{
	Name:       "void curse",
	ActionType: smt3.ProvideActionType(smt3.Curse),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void curse attack",
}

var voidExpel = domain.Skill{
	Name:       "void expel",
	ActionType: smt3.ProvideActionType(smt3.Expel),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void expel attack",
}

var voidDeath = domain.Skill{
	Name:       "void death",
	ActionType: smt3.ProvideActionType(smt3.Death),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "void death attack",
}

var physDrain = domain.Skill{
	Name:       "phys drain",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "drain physical attack",
}

var fireDrain = domain.Skill{
	Name:       "fire drain",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "drain fire attack",
}

var iceDrain = domain.Skill{
	Name:       "ice drain",
	ActionType: smt3.ProvideActionType(smt3.Ice),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "drain ice attack",
}

var forceDrain = domain.Skill{
	Name:       "force drain",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "drain force attack",
}

var elecDrain = domain.Skill{
	Name:       "elec drain",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "drain elec attack",
}

var physRepel = domain.Skill{
	Name:       "phys repel",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "repel physical attack",
}

var fireRepel = domain.Skill{
	Name:       "fire repel",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "repel fire attack",
}

var iceRepel = domain.Skill{
	Name:       "ice repel",
	ActionType: smt3.ProvideActionType(smt3.Ice),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "repel ice attack",
}

var forceRepel = domain.Skill{
	Name:       "force repel",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "repel force attack",
}

var elecRepel = domain.Skill{
	Name:       "elec repel",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Passive,
		Value: domain.NO_COST,
	},

	Target:   domain.Self,
	Game:     domain.Smt3,
	Dodge:    domain.Impossible,
	Critical: domain.Impossible,

	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "repel elec attack",
}

var attackAffinityPassives = map[NameSkill]domain.ImmutableSkill{
	AntiPhys:   domain.MakeImmutableSkill(&antiPhys),
	AntiFire:   domain.MakeImmutableSkill(&antiFire),
	AntiIce:    domain.MakeImmutableSkill(&antiIce),
	AntiForce:  domain.MakeImmutableSkill(&antiForce),
	AntiElec:   domain.MakeImmutableSkill(&antiElec),
	AntiMind:   domain.MakeImmutableSkill(&antiMind),
	AntiNerve:  domain.MakeImmutableSkill(&antiNerve),
	AntiCurse:  domain.MakeImmutableSkill(&antiCurse),
	AntiExpel:  domain.MakeImmutableSkill(&antiExpel),
	AntiDeath:  domain.MakeImmutableSkill(&antiDeath),
	VoidPhys:   domain.MakeImmutableSkill(&voidPhys),
	VoidFire:   domain.MakeImmutableSkill(&voidFire),
	VoidIce:    domain.MakeImmutableSkill(&voidIce),
	VoidForce:  domain.MakeImmutableSkill(&voidForce),
	VoidElec:   domain.MakeImmutableSkill(&voidElec),
	VoidMind:   domain.MakeImmutableSkill(&voidMind),
	VoidNerve:  domain.MakeImmutableSkill(&voidNerve),
	VoidCurse:  domain.MakeImmutableSkill(&voidCurse),
	VoidExpel:  domain.MakeImmutableSkill(&voidExpel),
	VoidDeath:  domain.MakeImmutableSkill(&voidDeath),
	PhysDrain:  domain.MakeImmutableSkill(&physDrain),
	FireDrain:  domain.MakeImmutableSkill(&fireDrain),
	IceDrain:   domain.MakeImmutableSkill(&iceDrain),
	ForceDrain: domain.MakeImmutableSkill(&forceDrain),
	ElecDrain:  domain.MakeImmutableSkill(&elecDrain),
	PhysRepel:  domain.MakeImmutableSkill(&physRepel),
	FireRepel:  domain.MakeImmutableSkill(&fireRepel),
	IceRepel:   domain.MakeImmutableSkill(&iceRepel),
	ForceRepel: domain.MakeImmutableSkill(&forceRepel),
	ElecRepel:  domain.MakeImmutableSkill(&elecRepel),
}
