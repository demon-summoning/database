package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func Tetraja() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 15}

	return &demon.Skill{
		Name:           "tetraja",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.AllAlly,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Tetrakarn() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 45}

	return &demon.Skill{
		Name:           "tetraja",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.AllAlly,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Makarakarn() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 45}

	return &demon.Skill{
		Name:           "makarakarn",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.AllAlly,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
