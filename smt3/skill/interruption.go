package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var persuade = domain.Skill{
	Name:       "persuade",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var nag = domain.Skill{
	Name:       "nag",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var flatter = domain.Skill{
	Name:       "flatter",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var intimidate = domain.Skill{
	Name:       "intimidate",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var gonnection = domain.Skill{
	Name:       "gonnection",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var haggle = domain.Skill{
	Name:       "haggle",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var detain = domain.Skill{
	Name:       "detain",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var maidensPlea = domain.Skill{
	Name:       "maiden's plea",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.KikuriHime)},
}

var arbitration = domain.Skill{
	Name:       "arbitration",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var wineParty = domain.Skill{
	Name:       "wine party",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.KikuriHime)},
}

var interruptions = map[NameSkill]domain.ImmutableSkill{
	Persuade:    domain.MakeImmutableSkill(&persuade),
	Nag:         domain.MakeImmutableSkill(&nag),
	Flatter:     domain.MakeImmutableSkill(&flatter),
	Intimidate:  domain.MakeImmutableSkill(&intimidate),
	Gonnection:  domain.MakeImmutableSkill(&gonnection),
	Haggle:      domain.MakeImmutableSkill(&haggle),
	Detain:      domain.MakeImmutableSkill(&detain),
	Arbitration: domain.MakeImmutableSkill(&arbitration),
	MaidensPlea: domain.MakeImmutableSkill(&maidensPlea),
	WineParty:   domain.MakeImmutableSkill(&wineParty),
}
