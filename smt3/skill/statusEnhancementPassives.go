package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	demon "gitlab.com/constraintAutomaton/demon-summoning/database/smt3/demon"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

func LifeBonus() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LifeBonus(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "life bonus",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func LifeGain() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LifeGain(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "life gain",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func LifeSurge() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LifeSurge(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "life surge",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func ManaGain() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.ManaGain(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "mana gain",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func ManaSurge() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.ManaSurge(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "mana surge",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func LifeAid() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LifeAid(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "life aid",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func ManaAid() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.ManaAid(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "mana aid",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func LifeRefill() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LifeRefill(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "life refill",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func ManaRefill() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.ManaRefill(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "mana refill",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func VictoryCry() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.VictoryCry(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "victory cry",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Watchful() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Watchful(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "watchful",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Counter() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Counter(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "counter",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Retaliate() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Retaliate(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "retaliate",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Avenge() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Avenge(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "avenge",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Might() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Might(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "might",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Endure() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Endure(),
		Percentage:   100,
	}
	raidou := demon.RaidouKuzunoha()
	return &demon.Skill{
		Name:           "endure",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: []*demon.Demon{raidou},
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func BrightMight() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.BrightMight(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "bright might",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func DarkMight() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.DarkMight(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "dark might",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func DrainAttack() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.DrainAttack(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "drain attack",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func AttackAll() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.AttackAll(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "attack all",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Pierce() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Pierce(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "pierce",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func NeverYield() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.NeverYield(),
		Percentage:   100,
	}
	dante := demon.Dante()
	return &demon.Skill{
		Name:           "never yield",
		ActionType:     smt3.StatusEnhance(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: []*demon.Demon{dante},
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}
