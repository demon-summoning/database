package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func Trade() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "trade",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Pester() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "pester",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Begging() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "begging",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Blackmail() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "blackmail",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func StoneHunt() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "stone hunt",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Loan() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: demon.NO_COST,
	}

	return &demon.Skill{
		Name:           "loan",
		ActionType:     smt3.ProvideActionType(smt3.Conversation),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
