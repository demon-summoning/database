package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var tarunda = domain.Skill{
	Name:       "tarunda",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 20},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Decreases all enemies' physical and magic-based attack power",
}

var sukunda = domain.Skill{
	Name:       "sukunda",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Decreases all enemies' Agility",
}

var rakunda = domain.Skill{
	Name:       "rakunda",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Decreases all enemies' Defense",
}

var warCry = domain.Skill{
	Name:       "war cry",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 40},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Drastically decreases all enemies' attack/magic capability; equal to casting Tarunda twice",
}

var fogBreath = domain.Skill{
	Name:       "fog breath",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 30},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Drastically decreases all enemies' Agility; equal to casting Sukunda twice",
}

var taunt = domain.Skill{
	Name:       "taunt",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 20},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Drastically lowers enemies' defense and raises their offense, equal to casting Rakunda twice and Tarukaja twice on all enemies",
}

var debilitate = domain.Skill{
	Name:       "debilitate",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 48},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Lowers enemies' offense, defense, and agility",
}

var provoke = domain.Skill{
	Name:       "provoke",
	ActionType: smt3.ProvideActionType(smt3.Debuff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST},

	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante),
		demon.ProvideADemon(demon.RaidouKuzunoha)},
	Description: "Drastically lowers enemies' defense and raises their offense; user recovers MP. Equal to casting Rakunda twice and Tarukaja twice on all enemies",
}

var debuffs = map[NameSkill]domain.ImmutableSkill{
	Tarunda:    domain.MakeImmutableSkill(&tarunda),
	Sukunda:    domain.MakeImmutableSkill(&sukunda),
	Rakunda:    domain.MakeImmutableSkill(&rakunda),
	WarCry:     domain.MakeImmutableSkill(&warCry),
	FogBreath:  domain.MakeImmutableSkill(&fogBreath),
	Taunt:      domain.MakeImmutableSkill(&taunt),
	Debilitate: domain.MakeImmutableSkill(&debilitate),
	Provoke:    domain.MakeImmutableSkill(&provoke),
}
