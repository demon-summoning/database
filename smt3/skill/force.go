package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var zan = domain.Skill{
	Name:       "zan",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var zanma = domain.Skill{
	Name:       "zanma",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 6},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var zandyne = domain.Skill{
	Name:       "zandyne",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var mazan = domain.Skill{
	Name:       "mazan",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 8},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var mazanma = domain.Skill{
	Name:       "mazanma",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var wingBuffet = domain.Skill{
	Name:       "wind buffet",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 9},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Light,
}

var tornado = domain.Skill{
	Name:       "tornado",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Heavy,
}

var windCutter = domain.Skill{
	Name:       "wind cutter",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 13},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
}

var hellExhaust = domain.Skill{
	Name:       "hell exhaust",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	Description:    "Moderate force damage to all enemies that removes all -kaja effects.Hell Biker's exclusive skill. ",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 1, Buff: domain.CancelEnemyBuff}},
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.HellBiker)},
}
var wetWind = domain.Skill{
	Name:       "wet wind",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Pazuzu)},
}
var whirlWind = domain.Skill{
	Name:       "whirlwind",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}
var hitokotoGust = domain.Skill{
	Name:       "hitokoto gust",
	ActionType: smt3.ProvideActionType(smt3.Force),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
}

var forces = map[NameSkill]domain.ImmutableSkill{
	Zan:          domain.MakeImmutableSkill(&zan),
	Zanma:        domain.MakeImmutableSkill(&zanma),
	Zandyne:      domain.MakeImmutableSkill(&zandyne),
	Mazan:        domain.MakeImmutableSkill(&mazan),
	Mazanma:      domain.MakeImmutableSkill(&mazanma),
	WingBuffet:   domain.MakeImmutableSkill(&wingBuffet),
	Tornado:      domain.MakeImmutableSkill(&tornado),
	WindCutter:   domain.MakeImmutableSkill(&windCutter),
	HellExhaust:  domain.MakeImmutableSkill(&hellExhaust),
	WetWind:      domain.MakeImmutableSkill(&wetWind),
	WhirlWind:    domain.MakeImmutableSkill(&whirlWind),
	HitokotoGust: domain.MakeImmutableSkill(&hitokotoGust),
}
