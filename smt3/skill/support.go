package skill

import (
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func Analyze() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 2}

	return &demon.Skill{
		Name:           "analyze",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Estoma() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 10}

	return &demon.Skill{
		Name:           "estoma",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Riberama() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 8}

	return &demon.Skill{
		Name:           "riberama",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Lightoma() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 10}

	return &demon.Skill{
		Name:           "lightoma",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Liftoma() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 12}

	return &demon.Skill{
		Name:           "liftoma",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Makatora() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 10}

	return &demon.Skill{
		Name:           "makatora",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.SingleAlly,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Trafuri() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 25}

	return &demon.Skill{
		Name:           "trafuri",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func BeckonCall() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Mp,
		Value: 20}

	return &demon.Skill{
		Name:           "beckon call",
		ActionType:     smt3.Support(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.None,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		MaxNumberOfHit: 1,
		Damage:         demon.No,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
