package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var talk = domain.Skill{
	Name:       "talk",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
	Description:    "Basic recruit command without extra effect",
}

var jiveTalk = domain.Skill{
	Name:       "jive talk",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
	Description:    "Allows Demi-fiend to recruit Foul/Haunt/Wilder clan demons under normal circumstance, instead of relying on encountering a 'dazed' demon during full Kagutsuchi phase",
}

var scout = domain.Skill{
	Name:       "scout",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Instant recruit : Adult talker to female target. Advert: Child talker to female target. ",
}

var kidnap = domain.Skill{
	Name:       "kidnap",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Instant recruit : Adult or youth talker to child target. Advert : Child talker to adult or youth target. ",
}

var beseech = domain.Skill{
	Name:       "beseech",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Talker is 10 levels lower than target (65%). Adverse: Talker is 10 levels higher than target (25%)",
}

var brainwash = domain.Skill{
	Name:       "brainwash",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Instant : Talker is 10 levels higher than target",
}

var wooing = domain.Skill{
	Name:       "wooing",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Young male talker to female target. Effective on female target if the female talker is older and 10 levels higher. Adverse: Male talker to male target",
}

var seduce = domain.Skill{
	Name:       "seduce",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Instant: Young female talker to male target, Adverse : Male talker to male target. ",
}

var darkPledge = domain.Skill{
	Name:       "dark pledge",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Instant : Performing during new Kagutsuchi phase",
}

var mischief = domain.Skill{
	Name:       "mischief",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Male talker to female target. Learned by Loki",
}

var soulRecruit = domain.Skill{
	Name:       "soul recruit",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Valkyrie)},
	Description:    "instant : Male target. Exclusive to Valkyrie",
}

var deathPact = domain.Skill{
	Name:       "death pact",
	ActionType: smt3.ProvideActionType(smt3.Conversation),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: domain.NO_COST,
	},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Mithra)},
}
var conversations = map[NameSkill]domain.ImmutableSkill{
	Talk:        domain.MakeImmutableSkill(&talk),
	JiveTalk:    domain.MakeImmutableSkill(&jiveTalk),
	Scout:       domain.MakeImmutableSkill(&scout),
	Kidnap:      domain.MakeImmutableSkill(&kidnap),
	Beseech:     domain.MakeImmutableSkill(&beseech),
	Brainwash:   domain.MakeImmutableSkill(&brainwash),
	Wooing:      domain.MakeImmutableSkill(&wooing),
	Seduce:      domain.MakeImmutableSkill(&seduce),
	DarkPledge:  domain.MakeImmutableSkill(&darkPledge),
	Mischief:    domain.MakeImmutableSkill(&mischief),
	SoulRecruit: domain.MakeImmutableSkill(&soulRecruit),
	DeathPact:   domain.MakeImmutableSkill(&deathPact),
}
