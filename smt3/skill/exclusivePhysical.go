package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var divineShot = domain.Skill{
	Name:       "divine shot",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 13},

	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var spiralViper = domain.Skill{
	Name:       "spiral viper",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 18},

	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var oniKagura = domain.Skill{
	Name:       "oni-kagura",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 30},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var deadlyFury = domain.Skill{
	Name:       "deadly fury",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 32},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.High,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var javelinRain = domain.Skill{
	Name:       "javelin rain",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 40},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Mute),
		Percentage:   20,
	}},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var xerosBeat = domain.Skill{
	Name:       "xeros beat",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 40},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Bind),
		Percentage:   30,
	}},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var gaeaRage = domain.Skill{
	Name:       "gaea rage",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 35},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var earthquake = domain.Skill{
	Name:       "earthquake",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 45},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Severe,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Skadi)},
}

var andalucia = domain.Skill{
	Name:       "andalucia",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 6},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 4,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Skadi)},
}

var hellSpin = domain.Skill{
	Name:       "hell spin",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 25},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.HellBiker)},
}

var terrorblade = domain.Skill{
	Name:       "terrorblade",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Panic),
		Percentage:   30,
	}},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 3,
	Damage:         domain.Light,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.HellBiker)},
}

var rebellion = domain.Skill{
	Name:       "rebellion",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 9},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.High,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}

var e_I = domain.Skill{
	Name:       "e&i",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 4,
	Damage:         domain.Light,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}

var bulletTime = domain.Skill{
	Name:       "bullet time",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Panic),
		Percentage:   30,
	}},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}

var boogieWoogie = domain.Skill{
	Name:       "boogie-woogie",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
}

var yoshitsuneShowUp = domain.Skill{
	Name:       "yoshitsune show-up",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 9},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.High,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
}

var mokoiBoomerang = domain.Skill{
	Name:       "mokoi boomerang",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Panic),
		Percentage:   30,
	}},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
}

var exclusivePhysicals = map[NameSkill]domain.ImmutableSkill{
	DivineShot:       domain.MakeImmutableSkill(&divineShot),
	SpiralViper:      domain.MakeImmutableSkill(&spiralViper),
	OniKagura:        domain.MakeImmutableSkill(&oniKagura),
	DeadlyFury:       domain.MakeImmutableSkill(&deadlyFury),
	JavelinRain:      domain.MakeImmutableSkill(&javelinRain),
	XerosBeat:        domain.MakeImmutableSkill(&xerosBeat),
	GaeaRage:         domain.MakeImmutableSkill(&gaeaRage),
	Earthquake:       domain.MakeImmutableSkill(&earthquake),
	Andalucia:        domain.MakeImmutableSkill(&andalucia),
	HellSpin:         domain.MakeImmutableSkill(&hellSpin),
	Terrorblade:      domain.MakeImmutableSkill(&terrorblade),
	Rebellion:        domain.MakeImmutableSkill(&rebellion),
	E_I:              domain.MakeImmutableSkill(&e_I),
	BulletTime:       domain.MakeImmutableSkill(&bulletTime),
	BoogieWoogie:     domain.MakeImmutableSkill(&boogieWoogie),
	YoshitsuneShowUp: domain.MakeImmutableSkill(&yoshitsuneShowUp),
	MokoiBoomerang:   domain.MakeImmutableSkill(&mokoiBoomerang),
}
