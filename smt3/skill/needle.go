package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func NeedleRush() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 10}

	return demon.Skill{
		Name:           "needle rush",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.NotProvided,
		MaxNumberOfHit: 1,
		Damage:         demon.Moderate,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func StunNeedle() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 6}

	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Stun(),
		Percentage:   40,
	}
	return demon.Skill{
		Name:           "stun needle",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.NotProvided,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func ToxicSting() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 7}

	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Poison(),
		Percentage:   40,
	}
	return demon.Skill{
		Name:           "toxic sting",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.NotProvided,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func AridNeedle() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 12}

	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Stone(),
		Percentage:   60,
	}

	return demon.Skill{
		Name:           "arid needle",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.NotProvided,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
