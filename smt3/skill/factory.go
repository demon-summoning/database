package skill

import domain "gitlab.com/demon-summoning/database"

func ProvideSkill(name NameSkill) *domain.ImmutableSkill {
	resp := skills[name]
	return &resp
}
