package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
)

var feralClaw = domain.Skill{
	Name:       "feral claw",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 6},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Low,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var venomClaw = domain.Skill{
	Name:       "venom claw",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 18},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Poison),
		Percentage:   40,
	}},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var stunClaw = domain.Skill{
	Name:       "stun claw",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 17},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Stun),
		Percentage:   40,
	}},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.Low,
	Critical:       domain.Medium,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var ironClaw = domain.Skill{
	Name:       "iron claw",
	ActionType: smt3.ProvideActionType(smt3.Physical),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 15},
	StatusEffect:   []domain.StatusEffectDescriptor{},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.High,
	Critical:       domain.High,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}
var claws = map[NameSkill]domain.ImmutableSkill{
	IronClaw:  domain.MakeImmutableSkill(&ironClaw),
	StunClaw:  domain.MakeImmutableSkill(&stunClaw),
	VenomClaw: domain.MakeImmutableSkill(&venomClaw),
	FeralClaw: domain.MakeImmutableSkill(&feralClaw),
}
