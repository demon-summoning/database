package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

func MindsEye() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.MindsEye(),
		Percentage:   100,
	}
	demiFiend := demon.DemiFiend()
	return &demon.Skill{
		Name:           "mind's eye",
		ActionType:     smt3.Misc(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: []*demon.Demon{demiFiend},
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func FastRetreat() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.FastRetreat(),
		Percentage:   100,
	}
	demiFiend := demon.DemiFiend()
	return &demon.Skill{
		Name:           "fast retreat",
		ActionType:     smt3.Misc(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: []*demon.Demon{demiFiend},
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func LuckyFind() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.LuckyFind(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "lucky find",
		ActionType:     smt3.Misc(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}

func Charisma() *demon.Skill {
	cost := demon.Cost{
		Unit:  demon.Passive,
		Value: demon.NO_COST,
	}
	statusEffect := demon.StatusEffectDescriptor{
		StatusEffect: smt3.Charisma(),
		Percentage:   100,
	}
	return &demon.Skill{
		Name:           "charisma",
		ActionType:     smt3.Conversation(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{statusEffect},
		Target:         demon.Self,
		Game:           domain.Smt3,
		Dodge:          demon.Impossible,
		Critical:       demon.Impossible,
		ExclusiveSkill: make([]*demon.Demon, 0),
		MaxNumberOfHit: 1,
		Damage:         demon.No,
	}
}
