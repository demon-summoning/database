package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

func allAilement() []domain.StatusEffectDescriptor {
	statusEffect := make([]domain.StatusEffectDescriptor, len(smt3.AllStatusEffect()))
	i := 0
	for k, v := range smt3.AllStatusEffect() {
		if k != smt3.Ko || k != smt3.Fly {
			statusEffect[i] = domain.StatusEffectDescriptor{
				StatusEffect: v,
				Percentage:   100,
			}
			i++
		}
	}
	return statusEffect
}

var dia = domain.Skill{
	Name:       "dia",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},

	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var media = domain.Skill{
	Name:       "media",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},

	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var diarama = domain.Skill{
	Name:       "diarama",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 7},

	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var mediarama = domain.Skill{
	Name:       "mediarama",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 20},

	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var diarahan = domain.Skill{
	Name:       "diarahan",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},

	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
}

var mediarahan = domain.Skill{
	Name:       "mediarahan",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 35},

	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
}

var posumidi = domain.Skill{
	Name:       "posumidi",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Poison),
		Percentage:   100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var mutudi = domain.Skill{
	Name:       "mutudi",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{{StatusEffect: smt3.ProvideStatusEffect(smt3.Mute),
		Percentage: 100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var paraladi = domain.Skill{
	Name:       "paraladi ",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Stun),
		Percentage:   100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var petradi = domain.Skill{
	Name:       "petradi",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Stone),
		Percentage:   100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var patra = domain.Skill{
	Name:       "patra",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{
		{
			StatusEffect: smt3.ProvideStatusEffect(smt3.Bind),
			Percentage:   100,
		}, {
			StatusEffect: smt3.ProvideStatusEffect(smt3.Sleep),
			Percentage:   100,
		}, {
			StatusEffect: smt3.ProvideStatusEffect(smt3.Panic),
			Percentage:   100,
		},
	},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var mePatra = domain.Skill{
	Name:       "me patra",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	StatusEffect: []domain.StatusEffectDescriptor{
		{
			StatusEffect: smt3.ProvideStatusEffect(smt3.Bind),
			Percentage:   100,
		}, {
			StatusEffect: smt3.ProvideStatusEffect(smt3.Sleep),
			Percentage:   100,
		}, {
			StatusEffect: smt3.ProvideStatusEffect(smt3.Panic),
			Percentage:   100,
		},
	},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
}

var recarm = domain.Skill{
	Name:       "recarm",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 20},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Ko),
		Percentage:   100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var samarecarm = domain.Skill{
	Name:       "samarecarm",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 35},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Ko),
		Percentage:   100,
	}},
	Target:         domain.SingleAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
}

var recarmdra = domain.Skill{
	Name:       "recarmdra",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: domain.SACRIFICE},
	StatusEffect: []domain.StatusEffectDescriptor{domain.StatusEffectDescriptor{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Ko),
		Percentage:   100,
	}},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
}

var prayer = domain.Skill{
	Name:       "prayer",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 50},
	StatusEffect:   allAilement(),
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
}

// add info
var holyMelody = domain.Skill{
	Name:       "prayer",
	ActionType: smt3.ProvideActionType(smt3.Healing),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 1},

	Target:         domain.RandomAll,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.Fully,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Trumpeter)},
}
var healings = map[NameSkill]domain.ImmutableSkill{
	Dia:        domain.MakeImmutableSkill(&dia),
	Media:      domain.MakeImmutableSkill(&media),
	Diarama:    domain.MakeImmutableSkill(&diarama),
	Mediarama:  domain.MakeImmutableSkill(&mediarama),
	Diarahan:   domain.MakeImmutableSkill(&diarahan),
	Mediarahan: domain.MakeImmutableSkill(&mediarahan),
	Posumidi:   domain.MakeImmutableSkill(&posumidi),
	Mutudi:     domain.MakeImmutableSkill(&mutudi),
	Paraladi:   domain.MakeImmutableSkill(&paraladi),
	Petradi:    domain.MakeImmutableSkill(&petradi),
	Patra:      domain.MakeImmutableSkill(&patra),
	MePatra:    domain.MakeImmutableSkill(&mePatra),
	Recarm:     domain.MakeImmutableSkill(&recarm),
	Samarecarm: domain.MakeImmutableSkill(&samarecarm),
	Recarmdra:  domain.MakeImmutableSkill(&recarmdra),
	Prayer:     domain.MakeImmutableSkill(&prayer),
	HolyMelody: domain.MakeImmutableSkill(&holyMelody),
}
