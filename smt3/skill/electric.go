package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

func shockStatusEffect() []domain.StatusEffectDescriptor {
	return []domain.StatusEffectDescriptor{{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Shock),
		Percentage:   domain.UNKNOWN,
	}}
}

var zio = domain.Skill{
	Name:       "zio",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var zionga = domain.Skill{
	Name:       "zionga",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 6},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var ziodyne = domain.Skill{
	Name:       "ziodyne",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var mazio = domain.Skill{
	Name:       "mazio",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 8},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var mazionga = domain.Skill{
	Name:       "mazionga",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var maziodyne = domain.Skill{
	Name:       "maziodyne",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var shock = domain.Skill{
	Name:       "shock",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 9},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var boltStorm = domain.Skill{
	Name:       "bolt storm",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect: []domain.StatusEffectDescriptor{{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Shock),
		Percentage:   25,
	}},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var roundTrip = domain.Skill{
	Name:       "round trip",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Dante)},
}

var mishagujiThunder = domain.Skill{
	Name:       "round trip",
	ActionType: smt3.ProvideActionType(smt3.Elec),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect:   shockStatusEffect(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
}

var electrics = map[NameSkill]domain.ImmutableSkill{
	Zio:              domain.MakeImmutableSkill(&zio),
	Zionga:           domain.MakeImmutableSkill(&zionga),
	Ziodyne:          domain.MakeImmutableSkill(&ziodyne),
	Mazio:            domain.MakeImmutableSkill(&mazio),
	Mazionga:         domain.MakeImmutableSkill(&mazionga),
	Maziodyne:        domain.MakeImmutableSkill(&maziodyne),
	Shock:            domain.MakeImmutableSkill(&shock),
	BoltStorm:        domain.MakeImmutableSkill(&boltStorm),
	RoundTrip:        domain.MakeImmutableSkill(&roundTrip),
	MishagujiThunder: domain.MakeImmutableSkill(&mishagujiThunder),
}
