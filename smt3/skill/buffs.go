package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var tarukaja = domain.Skill{
	Name:       "tarukaja",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases all allies' physical-based attacks",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 1, Buff: domain.AttackUp}},
}

var makakaja = domain.Skill{
	Name:       "makakaja",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases all allies' magical-based attacks",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 1, Buff: domain.MagicUp}},
}

var rakukaja = domain.Skill{
	Name:       "rakukaja",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases all allies' Defense",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 1, Buff: domain.DefenseUp}},
}

var sukukaja = domain.Skill{
	Name:       "sukukaja",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 12},
	Target:         domain.AllAlly,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Increases all allies' Agility",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 1, Buff: domain.AgilityUp}},
}

var focus = domain.Skill{
	Name:       "focus",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 5},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Charges own potential damage by 2.5 for the next Physical skill used",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 2.5, Buff: domain.AttackUp}},
}

var redCapote = domain.Skill{
	Name:       "red capote",
	ActionType: smt3.ProvideActionType(smt3.Buff),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 36},
	Target:         domain.Self,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Matador)},
	Description:    "Maximizes own Agility stats (Sukukaja x4)",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Multiplier: 4, Buff: domain.AgilityUp}},
}
var buffs = map[NameSkill]domain.ImmutableSkill{
	Tarukaja:  domain.MakeImmutableSkill(&tarukaja),
	Makakaja:  domain.MakeImmutableSkill(&makakaja),
	Rakukaja:  domain.MakeImmutableSkill(&rakukaja),
	Sukukaja:  domain.MakeImmutableSkill(&sukukaja),
	Focus:     domain.MakeImmutableSkill(&focus),
	RedCapote: domain.MakeImmutableSkill(&redCapote),
}
