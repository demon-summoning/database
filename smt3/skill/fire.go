package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var agi = domain.Skill{
	Name:       "agi",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var agilao = domain.Skill{
	Name:       "agilao",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 6},

	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var agidyne = domain.Skill{
	Name:       "agidyne",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var maragi = domain.Skill{
	Name:       "maragi",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 8},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var maragion = domain.Skill{
	Name:       "maragion",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var maragidyne = domain.Skill{
	Name:       "maragidyne",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var fireBreath = domain.Skill{
	Name:       "fire breath",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 9},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Light,
}

var hellFire = domain.Skill{
	Name:       "hell fire",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 20},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Moderate,
}

var prominence = domain.Skill{
	Name:       "prominence",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 30},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Heavy,
}

var ragnarok = domain.Skill{
	Name:       "ragnarok",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 30},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.Surt)},
}

var hellBurner = domain.Skill{
	Name:       "hell burner",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 8},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.HellBiker)},
}

var magmaAxis = domain.Skill{
	Name:       "magma axis",
	ActionType: smt3.ProvideActionType(smt3.Fire),
	Cost: domain.Cost{
		Unit:  domain.PercentageHp,
		Value: 30},
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Mega,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.DemiFiend)},
}

var fires = map[NameSkill]domain.ImmutableSkill{
	Agi:        domain.MakeImmutableSkill(&agi),
	Agilao:     domain.MakeImmutableSkill(&agilao),
	Agidyne:    domain.MakeImmutableSkill(&agidyne),
	Maragi:     domain.MakeImmutableSkill(&maragi),
	Maragion:   domain.MakeImmutableSkill(&maragion),
	Maragidyne: domain.MakeImmutableSkill(&maragidyne),
	FireBreath: domain.MakeImmutableSkill(&fireBreath),
	HellFire:   domain.MakeImmutableSkill(&hellFire),
	Prominence: domain.MakeImmutableSkill(&prominence),
	Ragnarok:   domain.MakeImmutableSkill(&ragnarok),
	HellBurner: domain.MakeImmutableSkill(&hellBurner),
	MagmaAxis:  domain.MakeImmutableSkill(&magmaAxis),
}
