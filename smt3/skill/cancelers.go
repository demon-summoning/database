package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
	demon "gitlab.com/demon-summoning/database/smt3/demon"
)

var dekunda = domain.Skill{
	Name:       "dekunda",
	ActionType: smt3.ProvideActionType(smt3.Canceler),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Cancels -nda effects",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Buff: domain.CancelDebuff}},
}

var dekaja = domain.Skill{
	Name:       "dekaja",
	ActionType: smt3.ProvideActionType(smt3.Canceler),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	Description:    "Cancels -kaja effects",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Buff: domain.CancelEnemyBuff}},
}

var holyStar = domain.Skill{
	Name:       "holy star",
	ActionType: smt3.ProvideActionType(smt3.Canceler),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon((demon.Dante))},
	Description:    "Cancels -nda effects",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Buff: domain.CancelDebuff}},
}

var guardianRaptor = domain.Skill{
	Name:       "guardian raptor",
	ActionType: smt3.ProvideActionType(smt3.Canceler),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.Impossible,
	Critical:       domain.Impossible,
	MaxNumberOfHit: 1,
	Damage:         domain.No,
	ExclusiveSkill: []*domain.ImmutableDemon{demon.ProvideADemon(demon.RaidouKuzunoha)},
	Description:    "Cancels -nda effects",
	Buff:           []domain.BuffDescriptor{domain.BuffDescriptor{Buff: domain.CancelDebuff}},
}

var cancelers = map[NameSkill]domain.ImmutableSkill{
	Dekunda:        domain.MakeImmutableSkill(&dekunda),
	Dekaja:         domain.MakeImmutableSkill(&dekaja),
	HolyStar:       domain.MakeImmutableSkill(&holyStar),
	GuardianRaptor: domain.MakeImmutableSkill(&guardianRaptor),
}
