package skill

import (
	demon "gitlab.com/constraintAutomaton/demon-summoning/database"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/demon-summoning/database/smt3"
)

func Lunge() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 10}
	return demon.Skill{
		Name:           "lunge",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func HellThrust() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 12}
	return demon.Skill{
		Name:           "hell thrust",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Single,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.Medium,
		MaxNumberOfHit: 1,
		Damage:         demon.Moderate,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Berserk() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 13}
	return demon.Skill{
		Name:           "hell thrust",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.Random,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.Low,
		MaxNumberOfHit: 5,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func Tempest() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 25}
	return demon.Skill{
		Name:           "tempest",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.All,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.Medium,
		MaxNumberOfHit: 5,
		Damage:         demon.Light,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}

func HadesBlast() demon.Skill {
	cost := demon.Cost{
		Unit:  demon.PercentageHp,
		Value: 32}
	return demon.Skill{
		Name:           "hades blast",
		ActionType:     smt3.Physical(),
		Cost:           cost,
		StatusEffect:   []demon.StatusEffectDescriptor{},
		Target:         demon.All,
		Game:           domain.Smt3,
		Dodge:          demon.NotProvided,
		Critical:       demon.High,
		MaxNumberOfHit: 5,
		Damage:         demon.Heavy,
		ExclusiveSkill: make([]*demon.Demon, 0),
	}
}
