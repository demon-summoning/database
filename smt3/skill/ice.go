package skill

import (
	domain "gitlab.com/demon-summoning/database"
	"gitlab.com/demon-summoning/database/smt3"
)

func freeze() []domain.StatusEffectDescriptor {
	return []domain.StatusEffectDescriptor{{
		StatusEffect: smt3.ProvideStatusEffect(smt3.Freeze),
		Percentage:   domain.UNKNOWN,
	}}
}

func iceType() *domain.ImmutableActionType {
	return iceType()
}

var bufu = domain.Skill{
	Name:       "bufu",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 3},
	StatusEffect:   freeze(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var bufula = domain.Skill{
	Name:       "bufula",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 6},
	StatusEffect:   freeze(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var bufudyne = domain.Skill{
	Name:       "bufudyne",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 10},
	StatusEffect:   freeze(),
	Target:         domain.Single,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var mabufu = domain.Skill{
	Name:       "mabufu",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 8},
	StatusEffect:   freeze(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Light,
}

var mabufula = domain.Skill{
	Name:       "mabufula",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 15},
	StatusEffect:   freeze(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Moderate,
}

var mabufudyne = domain.Skill{
	Name:       "mabufudyne",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect:   freeze(),
	Target:         domain.All,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: 1,
	Damage:         domain.Heavy,
}

var iceBreath = domain.Skill{
	Name:       "ice breath",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 9},
	StatusEffect:   freeze(),
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Light,
}

var glacialBlast = domain.Skill{
	Name:       "glacial blast",
	ActionType: iceType(),
	Cost: domain.Cost{
		Unit:  domain.Mp,
		Value: 25},
	StatusEffect:   freeze(),
	Target:         domain.Random,
	Game:           domain.Smt3,
	Dodge:          domain.NotProvided,
	Critical:       domain.NotProvided,
	MaxNumberOfHit: domain.UNKNOWN,
	Damage:         domain.Heavy,
}

var ices = map[NameSkill]domain.ImmutableSkill{
	Bufu:         domain.MakeImmutableSkill(&bufu),
	Bufula:       domain.MakeImmutableSkill(&bufula),
	Bufudyne:     domain.MakeImmutableSkill(&bufudyne),
	Mabufu:       domain.MakeImmutableSkill(&mabufu),
	Mabufula:     domain.MakeImmutableSkill(&mabufula),
	Mabufudyne:   domain.MakeImmutableSkill(&mabufudyne),
	IceBreath:    domain.MakeImmutableSkill(&iceBreath),
	GlacialBlast: domain.MakeImmutableSkill(&glacialBlast),
}
