package smt3

import (
	domain "gitlab.com/demon-summoning/database"
)

var mute = domain.StatusEffect{
	Name:   "mute",
	Effect: `Prevent any spell to be cast`,
	Game:   domain.Smt3,
}

var freeze = domain.StatusEffect{
	Name: "freeze",
	Effect: `Prevents any action in battle. 
	Usually caused by ice skills and generally only lasts for one turn.
	 In later games, attacking a frozen enemy is guaranteed to cause a critical hit.`,
	Game: domain.Smt3,
}

var shock = domain.StatusEffect{
	Name: "shock",
	Effect: `Prevents any action in battle.
	 Usually caused by electricity skills and generally only lasts for one turn. 
	 In later games, attacking a shocked enemy is guaranteed to cause a critical hit.`,
	Game: domain.Smt3}

var poison = domain.StatusEffect{
	Name: "poison",
	Effect: `Depletes health at the end of each turn,
	 often persisting on the field. Typically removed by Posumudi or Dis-Poison.`,
	Game: domain.Smt3}

var paralyze = domain.StatusEffect{
	Name: "paralyze",
	Effect: `Paralyzed characters are unable to move. 
	Unlike Shock, this often persists for more than one turn. 
	Typically removed by Paraladi or Dis-Paralyze. `,
	Game: domain.Smt3}

var stun = domain.StatusEffect{
	Name: "stun",
	Effect: `Stunned characters have lowered agility and defense. 
	Typically removed by Paraladi or Dis-Stun.`,
	Game: domain.Smt3}

var confused = domain.StatusEffect{
	Name: "confused",
	Effect: `Confused characters become indecisive and may either attack a random enemy,
	 do nothing or flee in a panic.`,
	Game: domain.Smt3}

var charm = domain.StatusEffect{
	Name: "charm",
	Effect: `Character begins attacking their own allies or cast healing spells on the enemies.
		 Typically removed with Charmdi or Dis-Charm. `,
	Game: domain.Smt3}

var stone = domain.StatusEffect{
	Name: "stone",
	Effect: `Prevents the target from moving.
	 This persists even on the field and is treated as similar to being dead.
	  In most games, if the protagonist is petrified, it is game over.
	   In some games, Force spells will shatter a petrified opponent. `,
	Game: domain.Smt3}

var sleep = domain.StatusEffect{
	Name: "sleep",
	Effect: `Prevents any action in battle. Character loses a turn. 
	In some games, sleeping characters restore some of their HP and MP.`,
	Game: domain.Smt3}

var ko = domain.StatusEffect{
	Name: "ko",
	Effect: `Can no longer participate in battle.
	 In most games, if the protagonist dies, it is game over. 
	 Can only be recovered with Recarm spells.`,
	Game: domain.Smt3}

var bind = domain.StatusEffect{
	Name:   "bind",
	Effect: "Prevents any action from a unit. ",
	Game:   domain.Smt3}

var fly = domain.StatusEffect{
	Name: "fly",
	Effect: `Renders the afflicted target with weakness to Expel attacks.
	 Only caused by Baal Avatar's Bael's Bane ability. Wears off after battle.`,
	Game: domain.Smt3}

var panic = domain.StatusEffect{
	Name:   "panic",
	Effect: "TO DO",
	Game:   domain.Smt3}

var attackDown = domain.StatusEffect{
	Name:   "attack down",
	Effect: "Decrease physical and magic-based attack power",
	Game:   domain.Smt3,
}

var attackUp = domain.StatusEffect{
	Name:   "attack up",
	Effect: "Increase physical-based attacks",
	Game:   domain.Smt3,
}

var magicAttackUp = domain.StatusEffect{
	Name:   "magic attack up",
	Effect: "increase magical-based attacks",
	Game:   domain.Smt3,
}

var agilityDown = domain.StatusEffect{
	Name:   "agility down",
	Effect: "decrease agility",
	Game:   domain.Smt3,
}

var agilityUp = domain.StatusEffect{
	Name:   "agility up",
	Effect: "increase agility",
	Game:   domain.Smt3,
}

var defenceDown = domain.StatusEffect{
	Name:   "defence down",
	Effect: "decrease defense",
	Game:   domain.Smt3,
}
var defenseUp = domain.StatusEffect{
	Name:   "defense up",
	Effect: "increase defence",
	Game:   domain.Smt3,
}

type NameStatusEffect int

const (
	Mute NameStatusEffect = iota
	Freeze
	Shock
	Poison
	Paralyse
	Stun
	Confused
	Charm
	Stone
	Sleep
	Ko
	Bind
	Fly
	Panic
	AttackDown
	AttackUp
	MagicAttackUp
	AgilityDown
	AgilityUp
	DefenceDown
	DefenceUp
)

var allStatusEffect = map[NameStatusEffect]domain.ImmutableStatusEffect{
	Mute:     domain.MakeImmutableStatusEffect(&mute),
	Freeze:   domain.MakeImmutableStatusEffect(&freeze),
	Shock:    domain.MakeImmutableStatusEffect(&shock),
	Poison:   domain.MakeImmutableStatusEffect(&poison),
	Paralyse: domain.MakeImmutableStatusEffect(&paralyze),
	Stun:     domain.MakeImmutableStatusEffect(&stun),
	Confused: domain.MakeImmutableStatusEffect(&confused),
	Charm:    domain.MakeImmutableStatusEffect(&charm),
	Stone:    domain.MakeImmutableStatusEffect(&stone),
	Sleep:    domain.MakeImmutableStatusEffect(&sleep),
	Ko:       domain.MakeImmutableStatusEffect(&ko),
	Bind:     domain.MakeImmutableStatusEffect(&bind),
	Fly:      domain.MakeImmutableStatusEffect(&fly),
	Panic:    domain.MakeImmutableStatusEffect(&panic),
}

func AllStatusEffect() map[NameStatusEffect]domain.ImmutableStatusEffect {
	return allStatusEffect
}

// Provide a statut effect if NameStatusEffect is not taken from the constant value undefined behavior will en suites
func ProvideStatusEffect(name NameStatusEffect) domain.ImmutableStatusEffect {
	return allStatusEffect[name]
}
