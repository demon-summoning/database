package smt3

import (
	object "gitlab.com/demon-summoning/database"
)

var physical = object.ActionType{
	ActionType: "physical",
	Game:       object.Smt3}

var almightyPhysical = object.ActionType{
	ActionType: "almighty physical",
	Game:       object.Smt3}

var healing = object.ActionType{
	ActionType: "healing",
	Game:       object.Smt3}

var mind = object.ActionType{
	ActionType: "mind",
	Game:       object.Smt3}

var nerve = object.ActionType{
	ActionType: "nerve",
	Game:       object.Smt3}

var curse = object.ActionType{
	ActionType: "curse",
	Game:       object.Smt3}

var support = object.ActionType{
	ActionType: "support",
	Game:       object.Smt3}

var fire = object.ActionType{
	ActionType: "fire",
	Game:       object.Smt3}

var ice = object.ActionType{
	ActionType: "ice",
	Game:       object.Smt3}

var elec = object.ActionType{
	ActionType: "elec",
	Game:       object.Smt3}

var force = object.ActionType{
	ActionType: "force",
	Game:       object.Smt3}

var almighty = object.ActionType{
	ActionType: "almighty",
	Game:       object.Smt3}

var death = object.ActionType{ActionType: "death",
	Game: object.Smt3}

var expel = object.ActionType{
	ActionType: "expel",
	Game:       object.Smt3}

var conversation = object.ActionType{
	ActionType: "conversation",
	Game:       object.Smt3}

var buff = object.ActionType{
	ActionType: "buff",
	Game:       object.Smt3}

var debuff = object.ActionType{
	ActionType: "debuff",
	Game:       object.Smt3}

var canceler = object.ActionType{
	ActionType: "canceler",
	Game:       object.Smt3}

var elementalStats = object.ActionType{
	ActionType: "resist/boost",
	Game:       object.Smt3}

var statusEnhance = object.ActionType{
	ActionType: "enhance",
	Game:       object.Smt3}

var misc = object.ActionType{
	ActionType: "misc",
	Game:       object.Smt3}

// Provide a statut effect if NameActionType is not taken from the constant value undefined behavior will en suites
func ProvideActionType(actionType NameActionType) *object.ImmutableActionType {
	res := allActionType[actionType]
	return &res
}

func AllActionType() []object.ImmutableActionType {
	allActionType := make([]object.ImmutableActionType, 0, len(allActionType))
	allActionType = append(allActionType, allActionType...)

	return allActionType
}

type NameActionType int

const (
	ElementalStats NameActionType = iota
	Conversation
	Expel
	Death
	Almighty
	Force
	Elec
	Ice
	Fire
	Support
	Curse
	Nerve
	Mind
	Healing
	Physical
	Buff
	Debuff
	Canceler
	StatusEnhance
	Misc
	AlmightyPhysical
)

var allActionType = map[NameActionType]object.ImmutableActionType{
	ElementalStats:   object.MakeImmutableActionType(&elementalStats),
	Conversation:     object.MakeImmutableActionType(&conversation),
	Expel:            object.MakeImmutableActionType(&expel),
	Death:            object.MakeImmutableActionType(&death),
	Almighty:         object.MakeImmutableActionType(&almighty),
	Force:            object.MakeImmutableActionType(&force),
	Elec:             object.MakeImmutableActionType(&elec),
	Ice:              object.MakeImmutableActionType(&ice),
	Fire:             object.MakeImmutableActionType(&fire),
	Support:          object.MakeImmutableActionType(&support),
	Curse:            object.MakeImmutableActionType(&curse),
	Nerve:            object.MakeImmutableActionType(&nerve),
	Mind:             object.MakeImmutableActionType(&mind),
	Healing:          object.MakeImmutableActionType(&healing),
	Physical:         object.MakeImmutableActionType(&physical),
	Buff:             object.MakeImmutableActionType(&buff),
	Debuff:           object.MakeImmutableActionType(&debuff),
	Canceler:         object.MakeImmutableActionType(&canceler),
	StatusEnhance:    object.MakeImmutableActionType(&statusEnhance),
	Misc:             object.MakeImmutableActionType(&misc),
	AlmightyPhysical: object.MakeImmutableActionType(&almightyPhysical)}
