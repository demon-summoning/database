package smt3

import (
	object "gitlab.com/demon-summoning/database"
)

var deity = object.Race{Game: object.Smt3, Race: "Deity"}

var megami = object.Race{Game: object.Smt3, Race: "Megami"}

var lady = object.Race{Game: object.Smt3, Race: "Lady"}

var fury = object.Race{Game: object.Smt3, Race: "Fury"}

var kishin = object.Race{Game: object.Smt3, Race: "Kishin"}

var holy = object.Race{Game: object.Smt3, Race: "Holy"}

var element = object.Race{Game: object.Smt3, Race: "Element"}

var mitama = object.Race{Game: object.Smt3, Race: "Mitama"}

var yoma = object.Race{Game: object.Smt3, Race: "Yoma"}

var fairy = object.Race{Game: object.Smt3, Race: "Fairy"}

var divine = object.Race{Game: object.Smt3, Race: "Divine"}

var fallen = object.Race{Game: object.Smt3, Race: "Fallen"}

var snake = object.Race{Game: object.Smt3, Race: "Snake"}

var beast = object.Race{Game: object.Smt3, Race: "Beast"}

var jirae = object.Race{Game: object.Smt3, Race: "Jirae"}

var brute = object.Race{Game: object.Smt3, Race: "Brute"}

var femme = object.Race{Game: object.Smt3, Race: "Femme"}

var vile = object.Race{Game: object.Smt3, Race: "Vile"}

var tyrant = object.Race{Game: object.Smt3, Race: "Tyrant"}

var night = object.Race{Game: object.Smt3, Race: "Night"}

var wilder = object.Race{Game: object.Smt3, Race: "Wilder"}

var haunt = object.Race{Game: object.Smt3, Race: "Haunt"}

var foul = object.Race{Game: object.Smt3, Race: "Fool"}

var seraph = object.Race{Game: object.Smt3, Race: "Seraph"}

var wargod = object.Race{Game: object.Smt3, Race: "Wargod"}

var genma = object.Race{Game: object.Smt3, Race: "Genma"}

var dragon = object.Race{Game: object.Smt3, Race: "Dragon"}

var avatar = object.Race{Game: object.Smt3, Race: "Avatar"}

var avian = object.Race{Game: object.Smt3, Race: "Avian"}

var raptor = object.Race{Game: object.Smt3, Race: "Raptor"}

var entity = object.Race{Game: object.Smt3, Race: "Entity"}

var fiend = object.Race{Game: object.Smt3, Race: "Fiend"}

var erthys = object.Race{Game: object.Smt3, Race: "Erthys"}

var aeros = object.Race{Game: object.Smt3, Race: "Aeros"}

var aquans = object.Race{Game: object.Smt3, Race: "Aquans"}

var flaemis = object.Race{Game: object.Smt3, Race: "Flaemis"}

var invalid = object.Race{Game: object.Smt3, Race: "Invalid"}

var up = object.Race{Game: object.Smt3, Race: "Up"}

var down = object.Race{Game: object.Smt3, Race: "Down"}

var special = object.Race{Game: object.Smt3, Race: "Special"}

type NameRace int

const (
	Deity NameRace = iota
	Megami
	Fury
	Lady
	Kishin
	Holy
	Yoma
	Fairy
	Divine
	Fallen
	Snake
	Beast
	Jirea
	Brute
	Femme
	Vile
	Tyrant
	Night
	Wilder
	Haunt
	Foul
	Seraph
	Wargod
	Genma
	Dragon
	Avatar
	Avian
	Raptor
	Entity
	Fiend
	Erthys
	Aeros
	Aquans
	Flaemis
)

func AllRace() []object.ImmutableRace {
	races := make([]object.ImmutableRace, 0, len(allStatusEffect))
	for _, val := range allRace {
		races = append(races, val)
	}
	return races
}

func ProvideRace(race NameRace) object.ImmutableRace {
	return allRace[race]
}

var allRace = map[NameRace]object.ImmutableRace{
	Deity:   object.MakeImmutableRace(&deity),
	Megami:  object.MakeImmutableRace(&megami),
	Fury:    object.MakeImmutableRace(&fury),
	Lady:    object.MakeImmutableRace(&lady),
	Kishin:  object.MakeImmutableRace(&kishin),
	Holy:    object.MakeImmutableRace(&holy),
	Yoma:    object.MakeImmutableRace(&yoma),
	Fairy:   object.MakeImmutableRace(&fairy),
	Divine:  object.MakeImmutableRace(&divine),
	Fallen:  object.MakeImmutableRace(&fallen),
	Snake:   object.MakeImmutableRace(&snake),
	Beast:   object.MakeImmutableRace(&beast),
	Jirea:   object.MakeImmutableRace(&jirae),
	Brute:   object.MakeImmutableRace(&brute),
	Femme:   object.MakeImmutableRace(&femme),
	Vile:    object.MakeImmutableRace(&vile),
	Tyrant:  object.MakeImmutableRace(&tyrant),
	Night:   object.MakeImmutableRace(&night),
	Wilder:  object.MakeImmutableRace(&wilder),
	Haunt:   object.MakeImmutableRace(&haunt),
	Foul:    object.MakeImmutableRace(&foul),
	Seraph:  object.MakeImmutableRace(&seraph),
	Wargod:  object.MakeImmutableRace(&wargod),
	Genma:   object.MakeImmutableRace(&genma),
	Dragon:  object.MakeImmutableRace(&dragon),
	Avatar:  object.MakeImmutableRace(&avatar),
	Avian:   object.MakeImmutableRace(&avian),
	Raptor:  object.MakeImmutableRace(&raptor),
	Entity:  object.MakeImmutableRace(&entity),
	Fiend:   object.MakeImmutableRace(&fiend),
	Erthys:  object.MakeImmutableRace(&erthys),
	Aeros:   object.MakeImmutableRace(&aeros),
	Aquans:  object.MakeImmutableRace(&aquans),
	Flaemis: object.MakeImmutableRace(&flaemis)}
