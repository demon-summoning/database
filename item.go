package database

type ItemType int

const (
	FusionItem ItemType = iota
	Healing
	Cure
	Incenses
	Revival
	Offensive
	Defensive
	NonCombat
	Special
)

type Item struct {
	Type        ItemType
	Name        string
	Description string
}

func DeathStone() Item {
	return Item{Type: FusionItem, Name: "Deathstone", Description: ""}
}
