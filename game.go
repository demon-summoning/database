package database

type Game int8

const (
	Smt3 Game = iota
)

func (g Game) String() string {
	return "Shin Megami Tensei 3 Nocturne"
}
