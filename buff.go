package database

type Buff int

const (
	AttackUp Buff = iota
	AttackDown
	DefenseUp
	DefenseDown
	MagicUp
	MagicDown
	AgilityUp
	AgilityDown
	CancelDebuff
	CancelEnemyBuff
)
