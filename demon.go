package database

type Demon struct {
	Name        string
	Stats       Stats
	Affinity    Affinity
	Race        *Race
	Hp          int
	Mp          int
	Lv          int
	Cost        int
	Game        Game
	Description string
	Skills      map[*Skill]Origin
	Image       string
}
