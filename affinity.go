package database

type Affinity struct {
	Null   []ActionType
	Repel  []ActionType
	Normal []ActionType
	Absorb []ActionType
	Game   Game
}

func (a Affinity) IsValid() bool {
	for _, el := range a.Null {
		if el.Game.String() != a.Game.String() {
			return false
		}
	}
	for _, el := range a.Repel {
		if el.Game.String() != a.Game.String() {
			return false
		}
	}
	for _, el := range a.Normal {
		if el.Game.String() != a.Game.String() {
			return false
		}
	}
	for _, el := range a.Absorb {
		if el.Game.String() != a.Game.String() {
			return false
		}
	}
	return true
}
